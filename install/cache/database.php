<!DOCTYPE html>
<html>
<head>
	<title>Elefant Installer</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script>
		function toggle (show) {
			if (show == 'sqlite') {
				document.getElementById ('sqlite').style.display = 'block';
				document.getElementById ('mysql').style.display = 'none';
			} else {
				document.getElementById ('sqlite').style.display = 'none';
				document.getElementById ('mysql').style.display = 'block';
			}
		}
		<?php if ($_POST['driver'] == 'mysql') { ?>
		window.onload = function () {
			toggle ('mysql');
		};
		<?php } ?>
	</script>
</head>
<body>
<div id="wrapper">
	<h1><span>Elefant</span> Installer</h1>
	
	<div id="steps">
		<ul>
			<li>Introduction</li>
			<li>License</li>
			<li>Requirements</li>
			<li class="active">Database</li>
			<li>Settings</li>
			<li>Finished</li>
		</ul>
	</div>

	<div id="body"><div id="content">

<?php if ($data->error) { ?>
<h3>Connection Error:</h3>
<p class="notice"><?php echo Template::sanitize ($data->error, 'UTF-8'); ?></p>
<?php } else { ?>
<h3>Database Connection</h3>
<?php } ?>

<?php if ($data->ready) { ?>
<p>Database tables created and settings saved.</p>

<p>Click "Next" to continue.</p>
<?php } else { ?>
<form method="POST" action="/install/?step=database">
<input type="hidden" name="file" value="../conf/site.db" />

<p>SQLite users can simply click "Connect &amp; Create Schema". MySQL users please select your driver and enter your server details to continue.</p>

<p>Driver:<br />
<select name="driver" onchange="toggle (this.options[this.selectedIndex].value)">
	<option value="sqlite">SQLite</option>
	<option value="mysql" <?php if ($_POST['driver'] == 'mysql') { ?>selected<?php } ?>>MySQL</option>
</select></p>

<div id="sqlite">
<p>File:<br /><tt>conf/site.db</tt></p>
</div>

<div id="mysql" style="display: none">
<p>Server:<br />
<input type="text" name="host" value="<?php echo Template::sanitize ($_POST['host'], 'UTF-8'); ?>" size="30" /></p>
<p>Port:<br />
<input type="text" name="port" value="<?php echo Template::sanitize ($_POST['port'], 'UTF-8'); ?>" size="15" /></p>
<p>Database:<br />
<input type="text" name="name" value="<?php echo Template::sanitize ($_POST['name'], 'UTF-8'); ?>" size="25" /></p>
<p>Username:<br />
<input type="text" name="user" value="<?php echo Template::sanitize ($_POST['user'], 'UTF-8'); ?>" size="25" /></p>
<p>Password:<br />
<input type="password" name="pass" value="<?php echo Template::sanitize ($_POST['pass'], 'UTF-8'); ?>" size="25" /></p>
</div>

<p><input type="submit" value="Connect &amp; Create Schema" /></p>
</form>
<?php } ?>

	</div></div>
	<?php if ($data->ready) { ?>
	<a class="next" href="/install/?step=settings">Next: Settings</a>
	<?php } ?>
</div>
</body>
</html>
